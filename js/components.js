class Title extends React.Component{
	render() {
		return <h2>{this.props.text}<br/><br/></h2>;
		
	}
}

class SearchForm extends React.Component{

	constructor(props){
		super();
		this.state = {
			user : props.user,
			includemembers : props.includemembers
		};
	}

	//We create functions to manage the events in the form and those changes are assigned to the state
	//It is not two ways data binding :(
	handleUser(ev){
		this.setState({user : ev.target.value});
	}

	handleIncludeMember(ev){
		this.setState({includemembers : ev.target.value});
	}

	handleSubmit(ev){
		ev.preventDefault();
		this.props.onSearch({
			user : this.state.user,
			includemembers : this.state.includemembers
		});
	}

	render() {
		return <form>
			<div className="container">

				<div className="row form-group">
					<div className="col-lg-3 col-lg-offset-4">
			        	<input type="text" className="form-control" placeholder="User" size="30" value={this.state.user} onChange={this.handleUser.bind(this)}/>
			        </div>
			        <div className="col-lg-1">
			        	<button type="button" className="btn btn-primary" onClick={this.handleSubmit.bind(this)}>Search</button>
			        </div>
			    </div>
			    <div className="row checkbox">
			        <label><input type="checkbox" value={this.state.includemembers} onChange={this.handleIncludeMember.bind(this)}/> Include projects where the user is member!</label>
			    </div>
		    </div>
		    <br/><br/>
		</form>;
	}
}

class ResultList extends React.Component{
	render() {
		var results = this.props.results;
		return <div className="col-lg-6 col-lg-offset-3 list-group">
			{
				results.map(function(result){
						return <ItemResult key={result.id} result={result}/>;
					}.bind(this)
				)
			}
		</div>;
	}
}

class ItemResult extends React.Component{
	render() {
		var result = this.props.result;
		return <a href={result.html_url} className="list-group-item">
			<div className="row">
				<div className="col-lg-4 text-left">
					<h4 className="list-group-item-heading">{result.name}</h4>
				</div>
				<div className="col-lg-8 text-left list-group-item-text">
						{result.description}
				</div>
			</div>
		</a>;
	}
}


class MainComponent extends React.Component{
	//The dinamic variables, the ones that we get through events, and result of events represent the state, 
	//and the main component has to have this state and pass it to the specific components
	constructor(props){
		super();
		this.state = {
			user : props.user,
			includemembers : props.includemembers,
			results : props.results
		};
	}

	searchResults(state){
		console.log(state.user+' - '+state.includemembers);

		var url = 'https://api.github.com/users/'+state.user+'/repos?sort=updated';
		if(state.includemembers){
			url += '&type=all';
		}

		fetch(url).then(function(response){
			if(response.ok){
				response.json().then(function(body){
					this.setState({results : body});
				}.bind(this));
			}else{
				this.setState({results : []});
			}
		}.bind(this));
	}

	changeSearchParams(state){
		this.setState(state);
		this.searchResults(state);
	}

	render() {
		return <div>
			<Title text='Git Repository Searcher'/>
			<SearchForm user={this.state.user} includemembers={this.state.includemembers} onSearch={this.changeSearchParams.bind(this)}/>
			<ResultList results={this.state.results}/>
		</div>;
		
	}
}

var results = [];
ReactDOM.render(<MainComponent  user="jj" includemembers={false} results={results}/>, document.getElementById('content'));


